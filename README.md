connect2tech.in-IdeaS-SpringFramework5x
=======================================

This is a sample project used for the Parleys WebDriver online courses. It contains starting points and solutions for the exercises in this course.


# The largest heading

## The second largest heading

###### The smallest heading

**This is bold text**

*This text is italicized*

~~This was mistaken text~~

**This text is _extremely_ important**

In the words of Abraham Lincoln:

> Pardon my French

Use `git status` to list all new or modified files that haven't yet been committed.

Some basic Git commands are:
```
git status
git add
git commit
```

This site was built using [GitHub Pages](https://pages.github.com/)

- George Washington
- John Adams
- Thomas Jefferson

1. James Madison
2. James Monroe
3. John Quincy Adams

- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request


# Important Links
- [Bitbucket Repositories](https://bitbucket.org/connect2tech)

## Spring Features, step-by-step

- com.c2t.di.App
- com.c2t.scope.App
- com.c2t.init.App
- com.c2t.bean.postprocess.App
- com.c2t.bean.parentchild.ParentChild
- com.c2t.di.constructor.App
- com.c2t.collection.App

## Autowiring using xml

- com.c2t.autowire.byname.App
- com.c2t.autowire.bytype.App
- com.c2t.autowire.byconstructor.App

## Autowiring using Annotation

- com.c2t.annotation.App
- com.c2t.annotation2.App

## AOP

- com.c2t.aop1.App

## EL

- in.connect2tech.el1
- in.connect2tech.el2
- in.connect2tech.el3


## Java Config

- in.connect2tech.javaconfig


## JDBC

- com.c2t.jdbc.JDBCEmployeeDAOImpl

## Spring-Hibernat Integration

- c2t-SpringHbmIntegration
- /c2t-SpringHbmIntegration/src/main/resources/spring/config/BeanLocations.xml