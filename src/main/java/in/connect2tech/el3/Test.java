package in.connect2tech.el3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Spring SPEL bean reference example.
 * 
 * @author codesjava
 */
public class Test {
	public static void main(String args[]) {
		// Get application context object.
		ApplicationContext context = new ClassPathXmlApplicationContext("in/connect2tech/el3/el3.xml");

		// Get studentBean.
		Student student = (Student) context.getBean("studentBean");

		// Print student properties.
		System.out.println("Name: " + student.getName());
		System.out.println("Street: " + student.getStreet());
		System.out.println("Postal: " + student.getPostcode());
		System.out.println("Country: " + student.getCountry());
	}
}