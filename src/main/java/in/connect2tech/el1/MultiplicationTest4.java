package in.connect2tech.el1;

import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

/**
 * Spring SPEL variable example.
 * 
 * @author codesjava
 */
public class MultiplicationTest4 {
	public static void main(String args[]) {
		// Create MulitplicationTest object.
		Mulitplication mulitplicationTest = new Mulitplication();

		// Create StandardEvaluationContext object
		// with MulitplicationTest object.
		StandardEvaluationContext context = new StandardEvaluationContext(mulitplicationTest);

		// Create a parser with default settings.
		ExpressionParser parser = new SpelExpressionParser();

		// Set variables values.
		parser.parseExpression("num1").setValue(context, "10");
		parser.parseExpression("num2").setValue(context, "20");

		// Calculate result.
		System.out.println(mulitplicationTest.multiplication());
	}
}