package in.connect2tech.el1;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * Spring SPEL "Hello World" example.
 * 
 * @author codesjava
 */
public class HelloTest1 {
	public static void main(String args[]) {
		// Create a parser with default settings.
		ExpressionParser parser = new SpelExpressionParser();

		// Parse the expression string and return an Expression object
		// which can be used for repeated evaluation.
		Expression exp = parser.parseExpression("'Hello World!'");

		// Evaluate this expression and returns the result.
		String message = exp.getValue(String.class);

		// Print result.
		System.out.println(message);
	}
}