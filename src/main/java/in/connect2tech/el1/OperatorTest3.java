package in.connect2tech.el1;

import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * Spring SPEL Operator test example.
 * 
 * @author codesjava
 */
public class OperatorTest3 {
	public static void main(String args[]) {
		// Create a parser with default settings.
		ExpressionParser parser = new SpelExpressionParser();

		// Arithmetic operator expressions.
		System.out.println(parser.parseExpression("10 * 20").getValue());
		System.out.println(parser.parseExpression("" + "'Today is: '+ new java.util.Date()").getValue());

		// Relational operator expressions.
		System.out.println(parser.parseExpression("10==5").getValue());

		// Logical operator expressions.
		System.out.println(parser.parseExpression("(10 > 5) and (5>3)").getValue());

	}
}