package com.c2t.aop.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyInvocationHandler implements InvocationHandler {

	private Object target;

	public MyInvocationHandler(Object target) {
		this.target = target;
	}

	public Object getTarget() {
		return target;
	}

	public void setTarget(Object target) {
		this.target = target;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] params) throws Throwable {
		long a = System.currentTimeMillis();
		Object result = method.invoke(target, params);
		System.out.println("total time taken  " + (System.currentTimeMillis() - a));
		return result;
	}

}