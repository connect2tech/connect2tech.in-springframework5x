package com.c2t.di;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 * 
 */
public class MainProgram {
	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("com/c2t/di/spring-di-3x.xml");
		Employee employee = (Employee)context.getBean("emp1");
		System.out.println(employee.getName());

		System.out.println(employee);
		
		
		Employee employee2 = (Employee)context.getBean("emp1");

		System.out.println(employee2);
	}
}
