package com.c2t.jdbctemplate;

public interface EmployeeDAO {

		public void insert(Employee employee);
		public Employee findById(int id);
}
